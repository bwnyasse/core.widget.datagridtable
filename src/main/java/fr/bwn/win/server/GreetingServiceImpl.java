package fr.bwn.win.server;

import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.bwn.win.client.GreetingService;
import fr.bwn.win.shared.PatientInfo;
import fr.bwn.win.shared.PatientInfoDataBase;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
GreetingService{

    public List<PatientInfo> getPatient() {
        //Simulate DataBase
        return PatientInfoDataBase.getPatient();
    }



}
