package fr.bwn.win.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootLayoutPanel;

import fr.bwn.win.client.datagrid.example.PatientClientTable;
import fr.bwn.win.client.datagrid.example.PatientServerTable;
import fr.bwn.win.client.datagrid.gin.MTableGinInjector;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class CoreWidgetTable implements EntryPoint {



    public static final MTableGinInjector INSTANCE = GWT.create(MTableGinInjector.class);
    
    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {

       PatientClientTable widget = new PatientClientTable();
//       PatientServerTable widget = new PatientServerTable();
       widget.setPagination(true);
       RootLayoutPanel.get().add(widget);        
        //RootLayoutPanel.get().add(new PatientInfoTable());
    }
    

}
