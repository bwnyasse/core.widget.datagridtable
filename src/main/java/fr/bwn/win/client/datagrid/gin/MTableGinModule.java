/**
 * 
 */
package fr.bwn.win.client.datagrid.gin;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.inject.client.AbstractGinModule;


/**
 * @author bwnyasse
 *
 */
public class MTableGinModule extends AbstractGinModule {


    @Override
    protected void configure() {
        
        //POC : Un bus d'evenement
       bind(EventBus.class).to(SimpleEventBus.class).asEagerSingleton();

    }
}
