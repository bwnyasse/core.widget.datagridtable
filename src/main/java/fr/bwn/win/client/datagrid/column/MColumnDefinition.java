package fr.bwn.win.client.datagrid.column;

import java.io.Serializable;
import java.util.Comparator;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;

public class MColumnDefinition<RowType extends Serializable , ColType extends Serializable> extends Column<RowType, ColType> {

    private int columnIndex = -1;
    private String  columnName;
    private String  tooltip;
    private boolean defaultShown = true;
    private boolean hidden       = false;

    private Header<Object> columnHeader = null;
    private Comparator<RowType> comparator = null;
    private MCellDefaultValueGetter<RowType, ColType> defaultValueGetter;
    

    public MColumnDefinition(AbstractCell<ColType> cell, MCellDefaultValueGetter<RowType, ColType> defaultValueGetter ,  String columnName) {

        this(cell, defaultValueGetter , columnName, columnName, null, true, true, false, null,null);
    }

    public MColumnDefinition(AbstractCell<ColType> cell, MCellDefaultValueGetter<RowType, ColType> defaultValueGetter ,  String columnName, String tooltip) {

        this(cell, defaultValueGetter , columnName, columnName, tooltip, true, true, false, null,null);
    }

    public MColumnDefinition(AbstractCell<ColType> cell, MCellDefaultValueGetter<RowType, ColType> defaultValueGetter , String fieldName, String columnName, String tooltip) {

        this(cell, defaultValueGetter , fieldName, columnName, tooltip, true, true, false, null,null);
    }

    public MColumnDefinition(AbstractCell<ColType> cell, MCellDefaultValueGetter<RowType, ColType> defaultValueGetter , String fieldName, String columnName, String tooltip, boolean defaultShown, boolean sortable, boolean hidden) {

        this(cell, defaultValueGetter , fieldName, columnName, tooltip, defaultShown, sortable, hidden, null,null);
    }
    
    public MColumnDefinition(AbstractCell<ColType> cell, MCellDefaultValueGetter<RowType, ColType> defaultValueGetter , String fieldName, String columnName, String tooltip, boolean defaultShown, boolean sortable, boolean hidden,Header<Object> columnHeader,MFieldUpdater<RowType, ColType>  fieldUpdater) {

        super(cell);

        this.columnName = columnName;
        this.tooltip = tooltip;
        this.defaultShown = defaultShown;
        this.hidden = hidden;
        this.columnHeader = columnHeader;
        this.defaultValueGetter = defaultValueGetter;

        setSortable(sortable);
        setDataStoreName(fieldName);
        
        //Utile pour la fonctionnalité d'edition d'une cellule.
        if(fieldUpdater != null){
            setFieldUpdater(fieldUpdater);
        }
    }


    /**
     * @return the columnIndex
     */
     public int getColumnIndex() {
         return columnIndex;
     }

     /**
      * @param columnIndex the columnIndex to set
      */
     public void setColumnIndex(int columnIndex) {
         this.columnIndex = columnIndex;
     }

     /**
      * @return the columnName
      */
     public String getColumnName() {
         return columnName;
     }

     /**
      * @param columnName the columnName to set
      */
     public void setColumnName(String columnName) {
         this.columnName = columnName;
     }

     /**
      * @return the tooltip
      */
     public String getTooltip() {
         return tooltip;
     }

     /**
      * @param tooltip the tooltip to set
      */
     public void setTooltip(String tooltip) {
         this.tooltip = tooltip;
     }

     /**
      * @return the defaultShown
      */
     public boolean isDefaultShown() {
         return defaultShown;
     }

     /**
      * @param defaultShown the defaultShown to set
      */
     public void setDefaultShown(boolean defaultShown) {
         this.defaultShown = defaultShown;
     }

     /**
      * @return the hidden
      */
     public boolean isHidden() {
         return hidden;
     }

     /**
      * @param hidden the hidden to set
      */
     public void setHidden(boolean hidden) {
         this.hidden = hidden;
     }

     /**
      * @return the columnHeader
      */
     public Header<Object> getColumnHeader() {
         return columnHeader;
     }

     /**
      * @return the comparator
      */
     public Comparator<RowType> getComparator() {
         return comparator;
     }

     /**
      * @param comparator the comparator to set
      */
     public void setComparator(Comparator<RowType> comparator) {
         this.comparator = comparator;
     }

     @Override
     public ColType getValue(RowType object) {
         return defaultValueGetter.getValue(object);
     }

}