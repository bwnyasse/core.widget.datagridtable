package fr.bwn.win.client.datagrid.poc_filter;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: a.kuchuk
 * Date: 23.01.12
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */
public class Person implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private int age;

    public Person(String name,String lastName,int age){
        this.name=name;
        this.lastName =lastName;
        this.age=age;
    }
    
    private String name;
    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
