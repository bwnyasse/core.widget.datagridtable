/**
 * 
 */
package fr.bwn.win.client.datagrid.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.LoadingStateChangeEvent;
import com.google.gwt.user.cellview.client.LoadingStateChangeEvent.LoadingState;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.bwn.win.client.datagrid.column.MColumnDefinition;
import fr.bwn.win.client.datagrid.column.header.MDefaultResizableHeader;
import fr.bwn.win.client.datagrid.pager.MSimplePager;
import fr.bwn.win.client.datagrid.pager.MSimplePager.MSimplePagerResources;

/**
 * Uniquement le visible , aucune manipulation des données.
 * - Controlle les  Colonne  & Tri. / Pagination / Filtre
 * TODO : Completer la JAVA 
 * 
 * @author bwnyasses
 * 
 */
public class MPagingFilterDataGrid<RowType extends Serializable> extends SimpleLayoutPanel{

    private List<MColumnDefinition<RowType, ? extends Serializable>> columnLists = new ArrayList<MColumnDefinition<RowType,? extends Serializable>>();

    private final MDataGrid<RowType> dataGrid = new MDataGrid<RowType>();

    private MSimplePager simplePager = null;

    /** controleur comprend le Loading Image + containerSimplePager **/
    private HorizontalPanel paginationControler = null;

    private HorizontalPanel containerSimplePager = null;
    private Image loadingImage;

    private boolean isFixedColumnWidth = false;

    /** Flag d'activation de la pagination. */ 
    private Boolean paginationActivated = true; // L"initialisation à true traduit surtout qu'on veut afficher le widget
    // de pagination , mais ne traduit pas si les boutons de controles 
    // sont visibles ou pas.


    public interface Resource extends CssResource{
        String gridData();
    }

    interface Bundle extends ClientBundle{

        public static final Bundle INSTANCE = GWT.create(Bundle.class);

        @Source("PagingFilterDataGrid.css")
        Resource css();

        ImageResource scrollTableLoading(); 
    }

    public Resource CSS = Bundle.INSTANCE.css();


    public MPagingFilterDataGrid() {
        CSS.ensureInjected();
        // addStyleName(CSS.gridContainer());

        setHeight("100%");
        dataGrid.setWidth("100%");
        dataGrid.addStyleName(CSS.gridData());

        initPagination();

        //Ne pas rafraichir l'entête chaque fois que les données changent
        dataGrid.setAutoHeaderRefreshDisabled(true);
        add(dataGrid);
    }

    /**
     * Initialisation du contrôleur de la pagination.
     */
    private void initPagination(){


        paginationControler = new HorizontalPanel();


        //FIXME : Modifier la decoration de la pagination.
        MSimplePagerResources pagerResources = GWT.create(MSimplePagerResources.class);
        simplePager = new MSimplePager(pagerResources);
        simplePager.setDisplay(dataGrid);
        simplePager.setVisible(paginationActivated);   

        containerSimplePager = new HorizontalPanel();
        containerSimplePager.add(simplePager);
        containerSimplePager.add(simplePager.getPagePanel());

//        loadingImage = new Image(Bundle.INSTANCE.scrollTableLoading());
//        loadingImage.setVisible(false);
//        paginationControler.add(loadingImage);
        paginationControler.add(containerSimplePager);

//        dataGrid.addLoadingStateChangeHandler(new LoadingStateChangeEvent.Handler() {
//
//            @Override
//            public void onLoadingStateChanged(LoadingStateChangeEvent event) {
//                LoadingState loadingState = event.getLoadingState();
//                if(loadingState == LoadingState.LOADING){
//                    loadingImage.setVisible(true);
//                }else if (loadingState == LoadingState.LOADED){
//                    loadingImage.setVisible(false);
//                }
//            }
//        });
    }

    public void changeLoadingVisibility(boolean visible)
    {
        loadingImage.setVisible(visible);
    }

    public void addColumnToDataGrid(MColumnDefinition<RowType,? extends Serializable> column ,Header<Object> columnHeader ){
        if(columnHeader == null){
            dataGrid.addColumn(column,new MDefaultResizableHeader<RowType>(this,column));
        }else{
            dataGrid.addColumn(column,columnHeader);
        }
        column.setColumnIndex(dataGrid.getColumnCount() -1);
        getColumns().add(column);

    }

    public void updateColumns() {
        if (dataGrid.getColumnCount() > 0) {
            //on nettoie la datagrid des colonnes actuelles.
            clearColumns();
        }
        for (MColumnDefinition<RowType,? extends Serializable> column : getColumns()) {
            Header<Object> columnHeader = column.getColumnHeader();
            if (!column.isHidden()) {
                addColumnToDataGrid(column,columnHeader);
            }
        }
    }

    public void clearColumns() {
        for (MColumnDefinition<RowType,? extends Serializable> column : getColumns()) {
            dataGrid.removeColumn(column);
        }
    }

    /**
     * @return the columns
     */
    public List<MColumnDefinition<RowType,? extends Serializable>> getColumns() {
        return columnLists;
    }


    /**
     * @return the dataGrid
     */
    public MDataGrid<RowType> getDataGrid() {
        return dataGrid;
    }

    /**
     * Méthode utilisée pour activer la pagination des résultats de la table.
     * 
     * @param activate
     *        flag a vrai pour activer la pagination.
     */
    public void setPagination(Boolean activate)
    {
        paginationActivated = activate;
        //FIXME: Gerer le cas d'une pagination avec le Scroll!
        if(activate != null){
            containerSimplePager.setVisible(activate);
        }
    }

    /**
     * Méthode utilisée pour savoir si le tableau à la pagination activé.
     * 
     * @return Vrai si la pagination est activé, faux sinon.
     */
    public Boolean isPaginable()
    {
        return paginationActivated;
    }

    /**
     * @return the paginationControler
     */
    public Widget getPaginationControler() {
        return paginationControler;
    }

    /**
     * @return the isFixedColumnWidth
     */
    public boolean isFixedColumnWidth() {
        return isFixedColumnWidth;
    }

    /**
     * @param isFixedColumnWidth the isFixedColumnWidth to set
     */
    public void setFixedColumnWidth(boolean isFixedColumnWidth) {
        this.isFixedColumnWidth = isFixedColumnWidth;
    }

    public Widget getPageRangeInformation(){
        return simplePager.getPageInformation();
    }

}
