/**
 * 
 */
package fr.bwn.win.client.datagrid.impl;

import com.google.gwt.user.cellview.client.DataGrid;

/**
 * @author bwnyasse
 *
 */
public interface MDataGridResources extends DataGrid.Resources {

    @Override
    @Source(value = {DataGrid.Style.DEFAULT_CSS, "MDataGrid.css"})
    DataGrid.Style dataGridStyle();
}
