package fr.bwn.win.client.datagrid.column.header;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public final class MDefaultResizeableHeaderCell extends AbstractCell<String> {

    public MDefaultResizeableHeaderCell() {
        super("click", "mousedown", "mousemove", "dblclick");
    }
    
    interface Templates extends SafeHtmlTemplates {
        
        //Template d'affichage de l'entête de la colonne.
        @SafeHtmlTemplates.Template("<div class=\"headerText\">{0}</div>")
        SafeHtml text(String value);

    }

    private static Templates templates = GWT.create(Templates.class);

    @Override
    public void render(Context context, String value, SafeHtmlBuilder sb) {
        if (value == null) {
            return;
        }
        // Rendering pour l'affichage de l'entête.
        SafeHtml renderedText = templates.text(value);
        sb.append(renderedText);
        
    }
}