package fr.bwn.win.client.datagrid.datasource.handler;

import java.io.Serializable;

import com.google.gwt.event.shared.EventHandler;

import fr.bwn.win.client.datagrid.MTable;

/**
 * Handler exécutée lorsque les données du tableau changent.
 * 
 * @author bwnyasse
 *
 * @param <T>
 */
public interface IDataSourceProviderChangeHandler<T extends Serializable> extends EventHandler
{
	/**
	 * Méthode exécutée quand les données d'un {@link MTable} changent.
	 * 
	 * @param event
	 *        L'event de changement des données.
	 */
	public void onDataSourceProviderChange(MDataSourceProviderChangeEvent<T> event);
}
