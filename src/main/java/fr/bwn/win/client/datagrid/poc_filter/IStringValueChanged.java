package fr.bwn.win.client.datagrid.poc_filter;

public interface IStringValueChanged {
    void valueChanged(String newValue);
}
