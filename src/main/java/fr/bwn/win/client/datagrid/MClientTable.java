/**
 * 
 */
package fr.bwn.win.client.datagrid;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;

import fr.bwn.win.client.datagrid.column.MColumnDefinition;
import fr.bwn.win.client.datagrid.datasource.MAbstractDataSourceProvider;
import fr.bwn.win.client.datagrid.datasource.impl.MClientDataSourceProvider;
import fr.bwn.win.client.datagrid.datasource.model.MDataSourceModel;

/**
 * Implémentation du tableau côté client. <p>
 * 
 * L'utilisation de ce tableau considère que toutes les donnés à gérer sont disponibles côté client. 
 * Il n'y a pas de chargement dynamique des données depuis le serveur.
 * 
 * <br/>
 * <b>Préconisation :</b> L'utiliser si on gère moins de 300 données.
 * 
 * @author bwnyasse
 *
 * @param <RowType>
 */
public abstract class MClientTable<RowType extends Serializable> extends MTable<RowType>{

    /** Le fournisseur de données **/
    private final MClientDataSourceProvider<RowType> dataSourceProvider = new MClientDataSourceProvider<RowType>();

    /**
     * Constructeur par défaut.
     */
    public MClientTable() {
        this(true,true);
    }
    
    /**
     * @param paginationActivated
     */
    public MClientTable(Boolean paginationActivated,boolean isFixedColumnWidth) {
        super(paginationActivated,isFixedColumnWidth);
        setDataSourceProvider(dataSourceProvider);
        
        //Connecte la grille des cellules au handler de tri.
        pagingFilterDataGrid.getDataGrid().addColumnSortHandler(dataSourceProvider.getSortHandler());
    }

    
    @Override
    public void addColumnToSortHandler(MColumnDefinition<RowType, ? extends Serializable> column){
        Comparator<RowType> comparator = column.getComparator();
        if(column.isSortable() &&  comparator != null){
            dataSourceProvider.getSortHandler().setComparator(column,comparator);
        }
    }

    /**
     * Méthode utilisée pour charger les données du tableau.
     * 
     * <p>
     * Cette méthode suppose qu'un fournisseur de données de type {@link MAbstractDataSourceProvider}a été déclaré et qu'on a une colonne à afficher.
     * </p>
     * 
     */
    public void load()
    {
        super.load();
        //TODO: Affiner l'implémentation quand les filtres seront cablés.
    }

    /**
     * Recharge les données du tableau et les réaffiche.
     * 
     * <p>
     *  A utiliser si le {@link MDataSourceModel} a été modifiée.
     * </p>
     */
    public void reLoad()
    {
        //TODO : Affiner
        //        Collection<T> tuples = getValue().getTuples();
        //        reLoad(tuples);
    }
    
    /**
     * Recharge les données du tableau et les réaffiche.
     * 
     * @param tuples
     *        Les données à insérer dans le tableau pour le recharger
     */
    public void reLoad(Collection<RowType> tuples)
    {
        setValue(tuples);
        //TODO: Affiner l'implémentation quand les filtres seront cablés.
    }
    
    /**
     * Méthode utilisée pour insérer une liste de données dans le tableau.
     * 
     * @param tuples
     *        Les données à insérer dans le tableau
     */
    public void setValue(Collection<RowType> tuples)
    {
        MDataSourceModel<RowType> model = new MDataSourceModel<RowType>();
        model.setSize(tuples.size());
        model.setTuples(tuples);

        //fournit au parent le modèle de données.
        setValue(model);
    }
}
