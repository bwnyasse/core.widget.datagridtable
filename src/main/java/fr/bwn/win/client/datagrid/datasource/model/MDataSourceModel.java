/**
 * 
 */
package fr.bwn.win.client.datagrid.datasource.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.google.gwt.dev.util.collect.Lists;

/**
 * Classe Modèle utilisée pour manipuler les données de la datasource du tableau. <br/>
 * 
 * <p>
 * Le Modèle comprend :
 * <li> Le nombre total de données.
 * <li> La {@link Collection} des données actuellement chargés.
 * </p>
 * <br/>
 * 
 * Ce Modèle est notamment transféré lors des évènements de type TODO: A completer  
 * 
 * @author bwnyasse
 * @param <RowType>
 */
public class MDataSourceModel<RowType extends Serializable> implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Le nombre de données dans la datasource. */
    private Integer size;

    /** La collection des données actuellement dans la datasource. */
    private Collection<RowType> tuples;

    /**
     * Constructeur par défaut.
     */
    public MDataSourceModel() {
        super();
        this.size = 0;
        this.tuples = new ArrayList<RowType>();
    }

    
    /**
     * @param tuples
     */
    public MDataSourceModel(Collection<RowType> tuples) {
        super();
        this.tuples = tuples;
        this.size = tuples.size();
    }


    /**
     * Constructeur du modèle de données. 
     * 
     * Idéal pour le tableau SERVEUR.
     * 
     * @param size
     *          Le nombre de données à gérér.
     * @param tuples
     *          la {@link Collection} des données du tableau.
     */
    public MDataSourceModel(Integer size, Collection<RowType> tuples) {
        super();
        this.size = size;
        this.tuples = tuples;
    }

    /**
     * Méthode utilisée pour récupérer le nombre de tuples de la table.
     * 
     * @return Le nombre de tuples de la table
     */
    public Integer getSize()
    {
        return size;
    }

    /**
     * Méthode utilisée pour spécifier le nombre de tuples de la table.
     * 
     * @param size
     *        Le nombre de tuples de la table
     */
    public void setSize(Integer size)
    {
        this.size = size;
    }
    
    /**
     * Méthode utilisée pour récupérer la collection des tuples de la table.
     * 
     * @return La collection des tuples de la table
     */
    public Collection<RowType> getTuples()
    {
        return tuples;
    }

    /**
     * Méthode utilisée pour spécifier la collection des tuples de la table.
     * 
     * @param data
     *        La collection des tuples de la table
     */
    public void setTuples(Collection<RowType> tuples)
    {
        this.tuples = tuples;
    }
}
