/**
 * 
 */
package fr.bwn.win.client.datagrid.column;

import java.io.Serializable;

/**
 *
 */
public interface MCellDefaultValueGetter<RowType extends Serializable,ColType extends Serializable>  {

    public ColType getValue(RowType object);
}
