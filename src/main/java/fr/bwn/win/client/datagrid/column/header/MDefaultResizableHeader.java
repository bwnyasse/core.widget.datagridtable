package fr.bwn.win.client.datagrid.column.header;

import java.io.Serializable;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;

import fr.bwn.win.client.datagrid.column.MColumnDefinition;
import fr.bwn.win.client.datagrid.impl.MPagingFilterDataGrid;

/**
 * PATCH GWT to enable ResizableHeader.
 * 
 * modificated by bwnyasse for MIPIH : 27/10/2013
 *
 * @param <RowType>
 */
public class MDefaultResizableHeader<RowType extends Serializable> extends Header<String> {

    private MColumnDefinition<RowType, ?> column ;
    private AbstractCellTable<RowType> cellTable;
    private String columnName = "";
    private static final int width = 20;
    private boolean isFixedColumnWidth = true;

    public MDefaultResizableHeader(MPagingFilterDataGrid<RowType> pfDataGrid, MColumnDefinition<RowType, ?> column) {
        super(new MDefaultResizeableHeaderCell());
        this.columnName = column.getColumnName();
        this.cellTable = pfDataGrid.getDataGrid();
        this.column = column;
        this.isFixedColumnWidth = pfDataGrid.isFixedColumnWidth();
    }

    @Override
    public String getValue() {
        return columnName;
    }

    @Override
    public void onBrowserEvent(Context context, Element target, NativeEvent event) {
        if(isFixedColumnWidth){
            super.onBrowserEvent(context, target, event);
        }else{
            String eventType = event.getType();
            if (eventType.equals("mousemove")) {
                new ColumnResizeHelper<RowType>(cellTable, column, target);
            } else {
                return;
            }
        }
    }

    private void setCursor(Element element, Cursor cursor) {
        element.getStyle().setCursor(cursor);
    }

    class ColumnResizeHelper<E> implements NativePreviewHandler {

        private HandlerRegistration handler;
        private AbstractCellTable<E> table;
        private Column<E, ?> col;
        private Element el;
        private boolean mousedown;
        private Element measuringElement;

        public ColumnResizeHelper(AbstractCellTable<E> table, Column<E, ?> col, Element el) {
            this.el = el;
            this.table = table;
            this.col = col;
            handler = Event.addNativePreviewHandler(this);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onPreviewNativeEvent(NativePreviewEvent event) {
            NativeEvent nativeEvent = event.getNativeEvent();
            nativeEvent.preventDefault();
            nativeEvent.stopPropagation();

            String eventType = nativeEvent.getType();
            int clientX = nativeEvent.getClientX();
            if (eventType.equals("mousemove") && mousedown) {
                int absoluteLeft = el.getAbsoluteLeft();
                int newWidth = clientX - absoluteLeft;
                newWidth = newWidth < width ? width : newWidth;
                table.setColumnWidth(col, newWidth + "px");
                return;
            }

            if (eventType.equals("mousemove") || eventType.equals("mousedown")) {
                Element eventTargetEl = nativeEvent.getEventTarget().cast();
                int absoluteLeft = eventTargetEl.getAbsoluteLeft();
                int offsetWidth = eventTargetEl.getOffsetWidth();
                if (clientX > absoluteLeft + offsetWidth - width) {
                    if (eventType.equals("mousedown")) {
                        mousedown = true;
                    } else {
                        setCursor(el, Cursor.COL_RESIZE);
                    }
                } else {
                    removeHandler();
                    return;
                }
            } else if (eventType.equals("mouseup")) {
                mousedown = false;
            } else if (eventType.equals("mouseout") && !mousedown) {
                removeHandler();
                return;
            }

            if (eventType.equals("dblclick")) {
                // Get column
                nativeEvent.preventDefault();
                nativeEvent.stopPropagation();
                double max = 0;
                startMeasuring();
                for (E t : table.getVisibleItems()) {
                    Object value = col.getValue(t);
                    SafeHtmlBuilder sb = new SafeHtmlBuilder();
                    Cell<Object> cell = (Cell<Object>) col.getCell();
                    cell.render(null, value, sb);
                    max = Math.max(measureText(sb.toSafeHtml().asString()), max);
                }
                finishMeasuring();
                table.setColumnWidth(col, (max + width) + "px");
                removeHandler();
            }
        }

        private void removeHandler() {
            handler.removeHandler();
            setCursor(el, Cursor.DEFAULT);
        }

        private void startMeasuring() {
            Document document = Document.get();
            measuringElement = document.createElement("div");
            measuringElement.getStyle().setPosition(Position.ABSOLUTE);
            measuringElement.getStyle().setLeft(-1000, Unit.PX);
            measuringElement.getStyle().setTop(-1000, Unit.PX);
            document.getBody().appendChild(measuringElement);
        }

        private double measureText(String text) {
            measuringElement.setInnerHTML(text);
            return measuringElement.getOffsetWidth();
        }

        private void finishMeasuring() {
            Document.get().getBody().removeChild(measuringElement);
        }
    }

    static class HeaderCell extends AbstractCell<String> {
        public HeaderCell() {
            super("click", "mousedown", "mousemove", "dblclick");
        }

        @Override
        public void render(Context context, String value, SafeHtmlBuilder sb) {
            sb.append(SafeHtmlUtils.fromString(value));
        }
    }
};