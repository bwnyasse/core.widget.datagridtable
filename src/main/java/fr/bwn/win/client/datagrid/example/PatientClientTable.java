/**
 * 
 */
package fr.bwn.win.client.datagrid.example;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.bwn.win.client.GreetingService;
import fr.bwn.win.client.GreetingServiceAsync;
import fr.bwn.win.client.datagrid.MClientTable;
import fr.bwn.win.client.datagrid.column.MCellDefaultValueGetter;
import fr.bwn.win.client.datagrid.column.MColumnDefinition;
import fr.bwn.win.shared.PatientInfo;

/**
 * @author bwnyasse
 *
 */
public class PatientClientTable extends MClientTable<PatientInfo>{

    
    private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
    
    /**
     * 
     */
    public PatientClientTable() {
        super(true,false);

        //Full Name
        MColumnDefinition<PatientInfo, String> fullNameColumn = new MColumnDefinition<PatientInfo, String>( new TextCell(),new MCellDefaultValueGetter<PatientInfo,String>(){

            public String getValue(PatientInfo object) {
                return object.getFullName();
            }
        }, "Nom Complet");
        addColumn(fullNameColumn);

        //First Name
        MColumnDefinition<PatientInfo, String> firstNameColumn = new MColumnDefinition<PatientInfo, String>(new TextCell(),  new MCellDefaultValueGetter<PatientInfo,String>(){

            public String getValue(PatientInfo object) {
                return object.getFirstName();
            }
        },"Prénom");
        firstNameColumn.setComparator(   new Comparator<PatientInfo>() {
            public int compare(PatientInfo o1, PatientInfo o2) {
                return o1.getFirstName().compareTo(o2.getFirstName());
            }
        });
        addColumn(firstNameColumn);

        //Last Name
        MColumnDefinition<PatientInfo, String> lastNameColumn = new MColumnDefinition<PatientInfo, String>(new TextCell(),  new MCellDefaultValueGetter<PatientInfo,String>(){

            public String getValue(PatientInfo object) {
                return object.getLastName();
            }
        },"Nom");
        lastNameColumn.setComparator(   new Comparator<PatientInfo>() {
            public int compare(PatientInfo o1, PatientInfo o2) {
                return o1.getLastName().compareTo(o2.getLastName());
            }
        });

        addColumn(lastNameColumn);
        
        //BirthDay
        MColumnDefinition<PatientInfo, String> birthDayColumn = new MColumnDefinition<PatientInfo, String>(new TextCell(), new MCellDefaultValueGetter<PatientInfo,String>(){

            public String getValue(PatientInfo object) {
                return object.getLastName();
            }
        }, "Date de naissance");
        addColumn(birthDayColumn);
        
        fetchData();
    }

    /**
     * @param patientClientTable
     */
    private void fetchData() {

        greetingService.getPatient(new AsyncCallback<List<PatientInfo>>() {
            
            @Override
            public void onSuccess(List<PatientInfo> result) {
                setValue(result);
            };
            
            @Override
            public void onFailure(Throwable caught) {
              System.out.println("ERREUR");
                // TODO Auto-generated method stub
                
            }
        });
    }

    
}
