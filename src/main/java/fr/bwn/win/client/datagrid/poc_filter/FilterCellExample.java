/**
 * 
 */
package fr.bwn.win.client.datagrid.poc_filter;

import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author bwnyasse
 *
 */
public class FilterCellExample extends VerticalPanel {

    public final FilteredListDataProvider<Person> personCollection = new FilteredListDataProvider<Person>(new IColumnFilter<Person>() {
        @Override
        public boolean isValid(Person value, String filter) {
            if(filter==null || value==null)
                return true;
            return value.getName().toLowerCase().contains(filter.toLowerCase()) ||
                    Integer.toString(value.getAge()).contains(filter.toLowerCase());
        }
    });
    
    public FilterCellExample(){

        TextBoxAdvanced filterBox=new TextBoxAdvanced();
        add(filterBox);

        CellTable<Person> cell=new CellTable<Person>();

        personCollection.getList().add(new Person("Art", "Shake", 21));
        personCollection.getList().add(new Person("Joe", "Black", 47));
        personCollection.getList().add(new Person("Sam", "PArish", 97));

        TextColumn<Person> namecolumn = new TextColumn<Person>() {
            @Override
            public String getValue(Person s) {
                return s.getName();
            }
        };

        TextColumn<Person> lastNameColumn = new TextColumn<Person>() {
            @Override
            public String getValue(Person s) {
                return s.getLastName();
            }
        };
        TextColumn<Person> ageColumn = new TextColumn<Person>() {
            @Override
            public String getValue(Person s) {
                return Integer.toString(s.getAge());
            }
        };

        cell.addColumn(namecolumn,"Name");
        cell.addColumn(lastNameColumn,"Last name");
        cell.addColumn(ageColumn,"Age");


        personCollection.addDataDisplay(cell);

        filterBox.addValueChangeHandler(new IStringValueChanged() {
            @Override
            public void valueChanged(String newValue) {
                personCollection.setFilter(newValue);
                personCollection.refresh();
            }
        });

        final Button sendButton = new Button("Send");
        final TextBox nameField = new TextBox();
        nameField.setText("GWT User");
        final Label errorLabel = new Label();

        // We can add style names to widgets
        sendButton.addStyleName("sendButton");


        // Add the nameField and sendButton to the RootPanel
        // Use RootPanel.get() to get the entire body element

        add(cell);
    }
}
