/**
 * 
 */
package fr.bwn.win.client.datagrid.datasource;

import java.io.Serializable;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.inject.Inject;

import fr.bwn.win.client.datagrid.MTable;
import fr.bwn.win.client.datagrid.datasource.handler.IDataSourceProviderChangeHandler;
import fr.bwn.win.client.datagrid.datasource.handler.MDataSourceProviderChangeEvent;
import fr.bwn.win.client.datagrid.datasource.model.MDataSourceModel;
import fr.bwn.win.client.datagrid.impl.MPagingFilterDataGrid;

/**
 * Classe abstraite de gestion des données chargées dans un {@link MTable}.<p>
 * 
 * Un {@link MAbstractDataSourceProvider} est gére les objects {@link MDataSourceModel} de 2 manières:
 * <li> Sur modification du modèle , notifie un {@link MTable} pour qu'il mette à jour les widgets.
 * <li> Peut être informé d'une modification de la datasource depuis le widget {@link MPagingFilterDataGrid}
 * <p>
 * 
 * @author bwnyasse
 */
public abstract class MAbstractDataSourceProvider<RowType extends Serializable> implements IDataSourceProvider<RowType>{

    @Inject 
    EventBus eventBus;

    /** Le modèle représentant les modèles à afficher **/
    private MDataSourceModel<RowType> dataSourceModel;

    /** Le manager de handler de la classe **/
    private final HandlerManager handlerManager;

    /**
     * @param dataProvider
     */
    public MAbstractDataSourceProvider() {
        super();
        
        this.handlerManager = new HandlerManager(this);

        // Initialise la dataSourceProvider avec un modèle de données vide.
        this.dataSourceModel = new MDataSourceModel<RowType>();
    }
    
    /**
     * Adds this handler to the widget.
     * 
     * @param <H>
     *        the type of handler to add
     * @param type
     *        the event type
     * @param handler
     *        the handler
     * @return {@link HandlerRegistration} used to remove the handler
     */
    protected final <H extends EventHandler> HandlerRegistration addHandler(final H handler,
            GwtEvent.Type<H> type)
    {
        return handlerManager.addHandler(type, handler);
    }

    /**
     * Communique un changement dans le modèle associé à la datasource.
     */
    protected void fireDataSourceChangeEvent(){
        // Notification.
        fireEvent(new MDataSourceProviderChangeEvent<RowType>(getDataSourceModel()));
    }

    public void fireEvent(GwtEvent<?> event)
    {
        if (handlerManager != null)
        {
            handlerManager.fireEvent(event);
        }
    }
    
    /**
     * Méthode pour ajouter un {@link IDataSourceProviderChangeHandler}.
     * 
     * @param handler
     *        Le handler à ajouter
     */
    public void addDataSourceProviderChangeHandler(IDataSourceProviderChangeHandler<RowType> handler)
    {
        addHandler(handler, MDataSourceProviderChangeEvent.TYPE);
    }

    /**
     * @return the dataSourceModel
     */
    public MDataSourceModel<RowType> getDataSourceModel() {
        return dataSourceModel;
    }

    public void setDataSourceModel(MDataSourceModel<RowType> model){
        //On met à jour la datasource 
        this.dataSourceModel = model;

        setData(model.getTuples());

        //Communique le changement de la datasource
        fireDataSourceChangeEvent();
    }



}
