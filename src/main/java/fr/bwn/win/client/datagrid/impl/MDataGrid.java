/**
 * 
 */
package fr.bwn.win.client.datagrid.impl;

import java.io.Serializable;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.cellview.client.AbstractPager;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.LoadingStateChangeEvent;
import com.google.gwt.user.cellview.client.LoadingStateChangeEvent.LoadingState;
import com.google.gwt.user.client.ui.HeaderPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.view.client.HasRows;
import com.google.gwt.view.client.RangeChangeEvent;

/**
 * Classe d'implémentation de la grille des cellules d'un {@link MPagingFilterDataGrid}.
 *
 * @author bwnyasse
 */
public class MDataGrid<RowType extends Serializable> extends DataGrid<RowType> {

    //FIXME: Bidouille interne pour simuler l'infinite scroll.
    public static final int INFINITE_SCROLL_PAGE_SIZE = 1000;

    public static final int DEFAULT_PAGESIZE = 50;

    private  ScrollPanel scrollPanel;

    private HandlerRegistration scrollHandlerRegistration = null;

    private  MScrollPager scrollPager = null ;

    /**
     * Constructeur par défaut.
     */
    public MDataGrid() {
        super(DEFAULT_PAGESIZE,GWT.<MDataGridResources> create(MDataGridResources.class));
        
        //  Bidouille interne pour récuperer le ScrollPanel dans lequel les données.
        HeaderPanel header = (HeaderPanel) getWidget();
        this.scrollPanel = (ScrollPanel) header.getContentWidget();
        this.scrollPager =  new MScrollPager(this);
        
        setLoadingIndicator(new Image(MPagingFilterDataGrid.Bundle.INSTANCE.scrollTableLoading()));
        //IMPORTANT PAGINATION TJR DISPONIBLE  , le MpagingFilter definit si on affiche les boutons de controle ou pas.
        setPagination(true);
    }


    
    /**
     * Méthode utilisée pour configurer la pagination avec le scroll.
     * @param activated
     *  Flag indicant si on pagine avec la scrollbar.
     */
    public void setUpScrollPagination(Boolean activated){
        // Force la page size à l'increment du scroll au cas où il yaura infinite scroll en cache.
        setPageSize(DEFAULT_PAGESIZE);
        
        // On n'a jamais defini la pagination avec le scroll
        if(activated){
            if(scrollHandlerRegistration == null){
                scrollPager.setUpScrollPagination();
            }
        }else{
            // Force la
            clearHanlder();
        }
        
        //TODO: Notification pour changer l'etat des elements de navigation de la pagination simple.
    }

    public void clearHanlder(){
        if(scrollHandlerRegistration != null){
            scrollHandlerRegistration.removeHandler();
            scrollHandlerRegistration = null;
        }
    }
    
    /**
     * Méthode utilisée pour spécifier si la pagination est à activer ou pas. 
     * 
     * @param activate
     *  flag indicant si la pagination est activée ou pas.
     */
    public void setPagination(Boolean activate)
    {
        if(activate){
            setPageSize(DEFAULT_PAGESIZE);
        }else{
            setPageSize(INFINITE_SCROLL_PAGE_SIZE);
        }
    }

    /**
     * Classe d'implémentation du widget responsable de la pagination avec la scrollbar du navigateur. <p>
     * 
     *  La {@link MScrollPager} met à jour la taille des données contenues dans {@link} lorsque la scrollbar atteint le fond.
     *
     * @author bwnyasse
     */
    class MScrollPager extends AbstractPager {

        private static final int DEFAULT_INCREMENT = MDataGrid.DEFAULT_PAGESIZE;

        private int incrementSize = DEFAULT_INCREMENT;

        private int lastPosition = 0;

        public MScrollPager(MDataGrid<RowType> mDataGrid) {
            setDisplay(mDataGrid);
        }

        //Nettoyage pour eviter les fuites mémoires.
        protected void onUnload() {
            clearHanlder();
        }
        
        public void setUpScrollPagination(){
            // Handler des "scroll events".
            scrollHandlerRegistration = scrollPanel.addScrollHandler(new ScrollHandler() {
                @Override
                public void onScroll(ScrollEvent event) {

                    //On ignore l'évènement si , la scrollbar est en haut.
                    int oldPosition = lastPosition;
                    lastPosition = scrollPanel.getVerticalScrollPosition();
                    if (oldPosition >= lastPosition) {
                        return;
                    }

                    HasRows display = getDisplay();
                    if (display == null) {
                        return;
                    }

                    int maxScrollTop = scrollPanel.getWidget().getOffsetHeight()
                            - scrollPanel.getOffsetHeight();
                    if (lastPosition >= maxScrollTop) {
                        //TODO : Faire un appel serveur.
                        // On incrémente la page.
                        int newPageSize = Math.min(
                                display.getVisibleRange().getLength() + incrementSize,
                                display.getRowCount());
                        display.setVisibleRange(0, newPageSize);
                    }
                }
            });
        }

        public int getIncrementSize() {
            return incrementSize;
        }

        public void setIncrementSize(int incrementSize) {
            this.incrementSize = incrementSize;
        }

        @Override
        protected void onRangeOrRowCountChanged() {
            //TODO
        }
    }
}
