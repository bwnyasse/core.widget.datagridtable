/**
 * 
 */
package fr.bwn.win.client.datagrid.sort;

import java.io.Serializable;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;

/**
 *
 */
public class MClientTableSortHandler<RowType extends Serializable> extends ListHandler<RowType>{

    /**
     * @param list
     */
    public MClientTableSortHandler(List<RowType> list) {
        super(list);
    }


}
