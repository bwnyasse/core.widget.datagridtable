/**
 * 
 */
package fr.bwn.win.client.datagrid;

import java.io.Serializable;
import java.util.Collection;

import fr.bwn.win.client.datagrid.column.MColumnDefinition;
import fr.bwn.win.client.datagrid.datasource.MAbstractDataSourceProvider;
import fr.bwn.win.client.datagrid.datasource.impl.MServerDataSourceProvider;
import fr.bwn.win.client.datagrid.datasource.model.MDataSourceModel;

/**
 * Implémentation du tableau côté serveur. <p>
 * 
 * L'utilisation de ce tableau induit l'utilisation du chargement dynamique des données depuis le serveur. 
 * 
 * @author bwnyasse
 *
 * @param <RowType>
 */
public class MServerTable <RowType extends Serializable> extends MTable<RowType>{

    /** Le fournisseur de données **/
    private final MServerDataSourceProvider<RowType> dataSourceProvider = new MServerDataSourceProvider<RowType>();

    /**
     * Constructeur par défaut.
     */
    public MServerTable() {
        this(true,true);
    }

    /**
     * @param paginationActivated
     */
    public MServerTable(Boolean paginationActivated,boolean isFixedColumnWidth) {
        super(paginationActivated,isFixedColumnWidth);
        setDataSourceProvider(dataSourceProvider);

        //Connecte la grille des cellules au handler de tri.
        //TODO: A implementer
    }


    @Override
    public void addColumnToSortHandler(MColumnDefinition<RowType, ? extends Serializable> column) {
        //TODO: A Implementer
    }

    /**
     * Méthode utilisée pour charger les données du tableau.
     * 
     * <p>
     * Cette méthode suppose qu'un fournisseur de données de type {@link MAbstractDataSourceProvider}a été déclaré et qu'on a une colonne à afficher.
     * </p>
     * 
     */
    public void load()
    {
        super.load();
        //TODO: Affiner l'implémentation quand les filtres seront cablés.
    }

    /**
     * Recharge les données du tableau et les réaffiche.
     * 
     * <p>
     *  A utiliser si le {@link MDataSourceModel} a été modifiée.
     * </p>
     */
    public void reLoad()
    {
        //TODO : Affiner
        //        Collection<T> tuples = getValue().getTuples();
        //        reLoad(tuples);
    }

    /**
     * Recharge les données du tableau et les réaffiche.
     * 
     * @param tuples
     *        Les données à insérer dans le tableau pour le recharger
     */
    public void reLoad(Collection<RowType> tuples)
    {
        setValue(tuples);
        //TODO: Affiner l'implémentation quand les filtres seront cablés.
    }

    /**
     * Méthode utilisée pour insérer une liste de données dans le tableau.
     * 
     * @param tuples
     *        Les données à insérer dans le tableau
     */
    public void setValue(Collection<RowType> tuples)
    {
        MDataSourceModel<RowType> model = new MDataSourceModel<RowType>();
        model.setSize(tuples.size());
        model.setTuples(tuples);

        //fournit au parent le modèle de données.
        setValue(model);
    }

    @Override
    public void setPagination(Boolean activate) {
        //FIXME: Pour le MServerTable, on manipule directement la pagination dans la grille des cellules à cause du scrolling.
        // TODO: Reflechir à un autre design pattern pour definir la pagination avec le serveur, sinon c'est compliqué à comprendre ... ? 
        pagingFilterDataGrid.setPagination(activate);
        pagingFilterDataGrid.getDataGrid().setUpScrollPagination(!activate);
    }
}
