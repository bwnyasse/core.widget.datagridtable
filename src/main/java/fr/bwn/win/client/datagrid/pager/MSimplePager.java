/**
 * 
 */
package fr.bwn.win.client.datagrid.pager;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;
import com.google.gwt.resources.client.CssResource.Shared;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.HasRows;
import com.google.gwt.view.client.Range;

import fr.bwn.win.client.datagrid.MTable;

/**
 * Classe d'implémentation du composant responsable de la pagination d'un {@link MTable}.
 * 
 * @author bwnyasse
 */
public class MSimplePager extends SimplePager{

    /** La textBox où l'utilisateur peut choisir une page **/
    private  final TextBox currentPageBox = new TextBox();

    /** Le label qui contien le nombre de pages **/
    private final Label numPagesLabel = new Label();

    /** Le label donnant les informations sur la page courante **/
    private final Label pageInformation = new Label();

    /** Le container des composants permettant de changer de pages **/ 
    private final HorizontalPanel pagePanel = new HorizontalPanel(); 
    
    /**
     * Interface référençant tous les styles du composant.
     * @author bwnyasse
     */
    @Shared
    @ImportedWithPrefix("MSimplePager")
    public interface Css extends CssResource
    {
        String mSimplePager();

        String mSimplePagerText();

        String mSimplePagerTextField();

        String mSimplePagerErrorMessage();
    }

    /**
     * Bundle d'accès aux styles et aux images.
     * @author bwnyasse
     */
    interface Bundle extends ClientBundle
    {
        public static final Bundle INSTANCE = GWT.create(Bundle.class);

        @Source("MSimplePager.css")
        Css css();
    }

    public static final Css CSS = Bundle.INSTANCE.css();


    /**
     *Bundle Resources pour surcharger les styles GWT.  
     * @author bwnyasse
     */
    public interface MSimplePagerResources extends SimplePager.Resources{

        /**
         * The image used to go to the first page.
         */
        @Source("first.png")
        @ImageOptions(flipRtl = true)
        ImageResource simplePagerFirstPage();


        /**
         * The image used to go to the last page.
         */
        @Source("last.png")
        @ImageOptions(flipRtl = true)
        ImageResource simplePagerLastPage();


        /**
         * The image used to go to the next page.
         */
        @Source("next.png")
        @ImageOptions(flipRtl = true)
        ImageResource simplePagerNextPage();


        /**
         * The image used to go to the previous page.
         */
        @Source("previous.png")
        @ImageOptions(flipRtl = true)
        ImageResource simplePagerPreviousPage();


        /**
         * The styles used in this widget.
         */
        @Source("MSimplePagerResources.css")
        Style simplePagerStyle();
    }

    public MSimplePager(Resources resources) {
        super(TextLocation.RIGHT, resources, false, 0, true);
        CSS.ensureInjected();
        addStyleName(CSS.mSimplePager());

        currentPageBox.addStyleName(CSS.mSimplePagerTextField());
        
        // Disallow non-numeric pages
        KeyPressHandler keyPressHandler = new KeyPressHandler()
        {
            public void onKeyPress(KeyPressEvent event)
            {
                //TODO : Rétablir MFieldsUtils.KEY_ENTER = 13 
                if (event.getNativeEvent().getKeyCode() == 13)
                {
                    gotoPage();
                }
            }
        };
        BlurHandler blurHandler = new BlurHandler()
        {
            public void onBlur(BlurEvent event)
            {
                gotoPage();
            }
        };

        currentPageBox.addKeyPressHandler(keyPressHandler);
        currentPageBox.addBlurHandler(blurHandler);
        numPagesLabel.addStyleName(CSS.mSimplePagerText());
        
        pagePanel.add(currentPageBox);
        pagePanel.add(numPagesLabel);
        
        // Cache la pagination s'il n'en faut pas.
        if(getPageCount() < 2){
            setVisible(false);
            pagePanel.setVisible(false);
        }
    }

    /**
     * Méthode utilisée pour déclencher la navigation demandée par l'utilisateur.
     */
    private void gotoPage()
    {
        //TODO: Utile ? 
        //        // On désactive les événements de selection. Comme ça les desélections due a la navigation ne
        //        // sont pas envoyées.
        //        getDataTable().setSelectionEventActive(false);
        
        // Si la valeur saisie n'est pas valide, on impose la valeur max de pages.
        if (getPagingBoxValue() >= getPageCount())
        {
            currentPageBox.setText(String.valueOf(getPageCount()));
        }
        setPage(getPagingBoxValue());
    }

    /**
     * Méthode utilisée pour récupérer la valeur de la page demandée par l'utilisateur.
     * 
     * @return
     *  l'index de la page demandée.
     */
    private int getPagingBoxValue()
    {
        int page = 0;
        try
        {
            page = Integer.parseInt(currentPageBox.getText()) - 1;
        }
        catch (NumberFormatException e)
        {
            // Si on arrive pas à parser l'entier , on remet la page 1.
            currentPageBox.setText("1");
        }

        // On remplace les valeurs < 1 par 1
        if (page < 1)
        {
            currentPageBox.setText("1");
            page = 0;
        }

        return page;
    }

    @Override
    protected void onRangeOrRowCountChanged() {
        super.onRangeOrRowCountChanged();

        if(getPageCount() < 2){
            pagePanel.setVisible(false);
            setVisible(false);
        }else{
            pagePanel.setVisible(true);
            setVisible(true);
        }
        
        pageInformation.setText(myCreateText()) ;
        pageInformation.getElement().getStyle().setFontWeight(FontWeight.BOLD);
        currentPageBox.setText(Integer.toString(getPage()+1));
        numPagesLabel.setText(" / " + getPageCount() );
    }

    /**
     * Get the text to display in the pager that reflects the state of the pager.
     *
     * @return the text
     */
    protected String myCreateText() {
        // Default text is 1 based.
        NumberFormat formatter = NumberFormat.getFormat("#,###");
        HasRows display = getDisplay();
        Range range = display.getVisibleRange();
        int pageStart = range.getStart() + 1;
        int pageSize = range.getLength();
        int dataSize = display.getRowCount();
        int endIndex = Math.min(dataSize, pageStart + pageSize - 1);
        endIndex = Math.max(pageStart, endIndex);
        return formatter.format(pageStart) + " - " + formatter.format(endIndex);
    }

    @Override
    protected String createText() {
        // Bidouille pour dissocier les boutons de navigations et les labels d'information 
        // En interne , le SimplePager GWT utilise cette méthode pour afficher les informations de la pagination.
        // On utilisera plutôt myCreateText() .
        return "";
    }

    /**
     * @return the pageInformation
     */
    public Label getPageInformation() {
        return pageInformation;
    }

    /**
     * @return the pagePanel
     */
    public HorizontalPanel getPagePanel() {
        return pagePanel;
    }
    
}
