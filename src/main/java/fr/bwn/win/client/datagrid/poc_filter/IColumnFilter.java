package fr.bwn.win.client.datagrid.poc_filter;

import java.io.Serializable;

/**
 * 
 * 
 * @author nyasse
 *
 * @param <RowType>
 *  Le type des données dans le tableau.
 */
public interface IColumnFilter<RowType extends Serializable> {
    
    boolean isValid(RowType value, String filteredValue);
}