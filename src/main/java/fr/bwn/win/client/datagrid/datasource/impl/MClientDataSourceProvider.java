/**
 * 
 */
package fr.bwn.win.client.datagrid.datasource.impl;

import java.io.Serializable;
import java.util.Collection;

import com.google.gwt.view.client.ListDataProvider;

import fr.bwn.win.client.datagrid.MClientTable;
import fr.bwn.win.client.datagrid.datasource.MAbstractDataSourceProvider;
import fr.bwn.win.client.datagrid.impl.MDataGrid;
import fr.bwn.win.client.datagrid.sort.MClientTableSortHandler;


/**
 * Gestionnaire de données uniquement côté client pour un {@link MClientTable}.
 * <p>clientDataProvider
 *  Cette classe encapsule un {@link ListDataProvider} pour toutes les manipulations.
 * @author bwnyasse
 * @param <RowType>
 */
public class MClientDataSourceProvider<RowType extends Serializable>  extends MAbstractDataSourceProvider<RowType>{

    /** Le dataProvider responsable de la gestion des données **/
    private final ListDataProvider<RowType> clientDataProvider;

    private MClientTableSortHandler<RowType> sortHandler;
    
    /**
     * Constructeur par défaut.
     */
    public MClientDataSourceProvider() {
        clientDataProvider = new ListDataProvider<RowType>();
        
        //Connecte le handler de tri avec la liste gérée par le fournisseur de données.
        sortHandler = new MClientTableSortHandler<RowType>(clientDataProvider.getList());
    }

    @Override
    public void addData(RowType data){
        if(data != null){
            clientDataProvider.getList().add(data);
        }
        //TODO: Notifier le changement de la datasource. ?
    }

    @Override
    public void removeData(RowType data){
        if(data != null){
            clientDataProvider.getList().remove(data);
        }
        //TODO: Notifier le changement de la datasource. ?
    }

    @Override
    public Integer getTotalRowsCount() {
        return clientDataProvider.getList().size();
    }

    @Override
    public void setData(Collection<RowType> data) {
        clientDataProvider.getList().addAll(data);
    }


    @Override
    public void connectToDataGrid(MDataGrid<RowType> dataGrid) {
        clientDataProvider.addDataDisplay(dataGrid);
    }

    /**
     * @return the sortHandler
     */
    public MClientTableSortHandler<RowType> getSortHandler() {
        return sortHandler;
    }


}
