/**
 * 
 */
package fr.bwn.win.client.datagrid;

import java.io.Serializable;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

import fr.bwn.win.client.datagrid.column.MColumnDefinition;
import fr.bwn.win.client.datagrid.datasource.MAbstractDataSourceProvider;
import fr.bwn.win.client.datagrid.datasource.handler.IDataSourceProviderChangeHandler;
import fr.bwn.win.client.datagrid.datasource.handler.MDataSourceProviderChangeEvent;
import fr.bwn.win.client.datagrid.datasource.model.MDataSourceModel;
import fr.bwn.win.client.datagrid.impl.MPagingFilterDataGrid;

/**
 * @author bwnyasse
 *
 * @param <RowType>
 */
public abstract  class MTable<RowType extends Serializable> extends DockLayoutPanel implements HasValue<MDataSourceModel<RowType>>{


    //************************** DATASOURCE **************************/
    private  MAbstractDataSourceProvider<RowType> dataSourceProvider = null;

    //************************** DATAGRID ***************************/
    protected MPagingFilterDataGrid<RowType> pagingFilterDataGrid;


    //************************  DECORATION PHYSIQUE DU TABLEAU *********/

    private Label numberAllItems =  new Label();
    private Label numberSelectedColumns = new Label() ;
    
    private Label pageInformation = new Label();
    
    //******************** CONSTRUCTEUR ******************/
    /**
     * Constructeur du tableau avec pagination.
     */
    public MTable(){
        this(true,true);
    }

    /**
     * Constructeur du tableau.
     * 
     * @param paginationActivated
     *        Flag pour préciser si la pagination est activée.
     */
    public MTable(Boolean paginationActivated,boolean isFixedColumnWidth)
    {
        super(Unit.PX);
        setHeight("100%");

        pagingFilterDataGrid = new MPagingFilterDataGrid<RowType>();
        pagingFilterDataGrid.setFixedColumnWidth(isFixedColumnWidth);
        
        setPagination(paginationActivated);

        //Toolbar
        HorizontalPanel headerContainerPanel = new HorizontalPanel();
        //Right toolBar
        HorizontalPanel rightPanel = new HorizontalPanel();
        headerContainerPanel.add(rightPanel);

        // Affichage du nombre de données dans le tableau et le nombre de données selectionnées.
        rightPanel.add(new Label("TOTAL : "));
        numberAllItems.setText("0");
        rightPanel.add(numberAllItems);
        rightPanel.add(new Label("Sél :"));
        numberSelectedColumns.setText("0");
        rightPanel.add(numberSelectedColumns);
        rightPanel.getElement().setAttribute("cellpadding", "5");
        rightPanel.add(pagingFilterDataGrid.getPageRangeInformation());
        
        //Left toolBar
        HorizontalPanel leftPanel = new HorizontalPanel();
        headerContainerPanel.add(leftPanel);
        headerContainerPanel.getElement().getStyle().setPaddingTop(5, Unit.PX);
        leftPanel.add(pagingFilterDataGrid.getPaginationControler());
        

        addNorth(headerContainerPanel, 40);
        add(pagingFilterDataGrid);
    }

    //************************  Private Methods *************************************/


    /**
     * Méthode utilisée pour notifier que l'affichage a changé.
     */
    private final void firePageChange()
    {
        //TODO: A Implémenter
    }


    //************************  Public Methods **************************************/
    /**
     *  <b>IMPORTANT: </b>A rédéfinir pour la politique de tri. <br/>
     * Méthode utilisée pour ajouter une colonne à la gestion du tri.<br/>
     * Le tri dépend de la politique de gestion du datasource : Client ou Serveur
     */
    public abstract void addColumnToSortHandler(MColumnDefinition<RowType, ? extends Serializable> column);

    /**
     * Méthode utilisée pour ajouter une colonne au tableau.
     * @param column
     *  la colonne à ajouter.
     */
    public void addColumn(MColumnDefinition<RowType, ? extends Serializable> column){
        addColumn(column,column.getColumnHeader());
    }

    /**
     * Méthode utilisée pour ajouter une colonne au tableau en spécifiant son entête.
     * @param column
     *  la colonne à ajouter.
     * @param columnHeader
     *  l'entête de la colonne.
     */
    public void addColumn(MColumnDefinition<RowType, ? extends Serializable> column,Header<Object> columnHeader){
        if(column == null){
            //TODO throws Error
        }
        pagingFilterDataGrid.addColumnToDataGrid(column,columnHeader);
        addColumnToSortHandler(column);
    }

    /**
     * Méthode utilisée pour charger les données du tableau.
     * 
     * <p>
     * Cette méthode suppose qu'un fournisseur de données de type {@link MAbstractDataSourceProvider}a été déclaré et qu'on a une colonne à afficher.
     * </p>
     * 
     */
    public void load() {

        if(dataSourceProvider  != null){
            // IMPORTANT A FAIRE : Connecte physiquement la grille des cellules du tableau à l'implémentation concrète du fournisseur de données.
            dataSourceProvider.connectToDataGrid(pagingFilterDataGrid.getDataGrid());

            // S'abonne aux changements quelconques de données gérées par le fournisseur de données au tableau. 
            // L'objectif est mettre à jour les labels d'information du tableau ( Nombre de lignes par exemple ). 
            dataSourceProvider.addDataSourceProviderChangeHandler(new IDataSourceProviderChangeHandler<RowType>() {
                @Override
                public void onDataSourceProviderChange(MDataSourceProviderChangeEvent<RowType> event) {
                    numberAllItems.setText(dataSourceProvider.getTotalRowsCount().toString());

                    // Propage l'évènement de changement.
                    MTable.this.fireEvent(event);
                }
            });

            // Informer des changements aux labels du tableau.
            firePageChange();
        }else{
            //TODO / Notifier une erreur pour dire qu'il faut absolument definir une datasource.
        }
    }    

    //************************ [Debut] GESTION HASVALUE SUR LE DATASOURCEMODEL ******************************************//
    @Override
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<MDataSourceModel<RowType>> handler) {
        return addHandler(handler, ValueChangeEvent.getType());
    }

    @Override
    public MDataSourceModel<RowType> getValue() {
        return dataSourceProvider.getDataSourceModel();
    }

    @Override
    public void setValue(MDataSourceModel<RowType> value) {
        setValue(value, true);
    }

    @Override
    public void setValue(MDataSourceModel<RowType> value, boolean fireEvents) {
        if(dataSourceProvider != null){
            dataSourceProvider.setDataSourceModel(value);
            if (fireEvents)
            {
                ValueChangeEvent.fire(this, value);
            }
        }
    }
    //************************ [Fin] GESTION HASVALUE SUR LE DATASOURCEMODEL ******************************************//

    //************************ [Début] GETTERS / SETTERS **************************************************/

    /**
     * Méthode utilisée pour activer la pagination des résultats du tableau.
     * 
     * @param activate
     *        flag à vrai pour activer la pagination.
     */
    public void setPagination(Boolean activate)
    {
        pagingFilterDataGrid.setPagination(activate);
    }

    /**
     * Méthode utilisée pour savoir si le tableau a la pagination activée.
     * 
     * @return Vrai si la pagination est activée, faux sinon.
     */
    public Boolean isPaginable()
    {
        return pagingFilterDataGrid.isPaginable();
    }

    /**
     * Méthode utilisée pour récupérer le fournisseur de données du tableau.
     * 
     * @return la source de données du tableau.
     */
    public MAbstractDataSourceProvider<RowType> getDataSourceProvider() {
        return dataSourceProvider;
    }


    /**
     * Méthode utilisée pour specifier le fournisseur de données du tableau.
     * 
     * @param dataSource
     *        le fournisseur de données du tableau.
     */
    public final void setDataSourceProvider(MAbstractDataSourceProvider<RowType> dataSourceProvider)
    {
        this.dataSourceProvider = dataSourceProvider;

        //IMPORTANT
        // On recharge le tableau après modification du fournisseur de données.
        // A ce niveau , le load doit être appelé une seule fois , car on definit la datasource une seule fois pour un tableau.
        load();
    }

    

    //*********************** [Fin] GETTERS / SETTERS ****************************************************/
}
