package fr.bwn.win.client.datagrid.datasource.handler;

import java.io.Serializable;

import com.google.gwt.event.shared.GwtEvent;

import fr.bwn.win.client.datagrid.datasource.MAbstractDataSourceProvider;
import fr.bwn.win.client.datagrid.datasource.model.MDataSourceModel;

/**
 * Evènement permettant de notifier que la {@link MAbstractDataSourceProvider} a été modifié.
 * 
 * @author bwnyasse
 *
 * @param <RowType>
 */
public class MDataSourceProviderChangeEvent<RowType extends Serializable> extends
GwtEvent<IDataSourceProviderChangeHandler<RowType>>
{   
    //Le type d'évènements.
    public static final Type<IDataSourceProviderChangeHandler<?>> TYPE  = new Type<IDataSourceProviderChangeHandler<?>>();

    private final MDataSourceModel<RowType> dataSourceModel;

    public MDataSourceProviderChangeEvent(MDataSourceModel<RowType> dataSourceModel)
    {
        super();
        this.dataSourceModel = dataSourceModel;
    }

    @Override
    protected void dispatch(IDataSourceProviderChangeHandler<RowType> handler)
    {
        handler.onDataSourceProviderChange(this);
    }


    /**
     * @return the dataSourceModel
     */
    public MDataSourceModel<RowType> getDataSourceModel() {
        return dataSourceModel;
    }


    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<IDataSourceProviderChangeHandler<RowType>> getAssociatedType() {
        return (Type)TYPE ;
    }
}
