/**
 * 
 */
package fr.bwn.win.client.datagrid.datasource.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;

import fr.bwn.win.client.datagrid.MServerTable;
import fr.bwn.win.client.datagrid.datasource.MAbstractDataSourceProvider;
import fr.bwn.win.client.datagrid.impl.MDataGrid;

/**
 * Gestionnaire de données uniquement côté serveur pour un {@link MServerTable}.
 * <p>
 *  Cette classe encapsule un {@link AsyncDataProvider} pour toutes les manipulations.
 * @author bwnyasse
 * @param <RowType>
 */
public class MServerDataSourceProvider<RowType extends Serializable> extends MAbstractDataSourceProvider<RowType>{

    /** Le dataProvider responsable de la gestion des données **/
    private final MAsyncDataProvider serverDataProvider;

    /** Liste pour simuler le fonctionnement d'une lazy list pour le chargement dynamique du tableau **/
    private List<RowType> lazyList ;

    class MAsyncDataProvider extends AsyncDataProvider<RowType> {

        @Override
        protected void onRangeChanged(HasData<RowType> dataGrid) {
            final Range range = dataGrid.getVisibleRange();
            final int start = range.getStart();
            final int end = start + range.getLength();
            if(!lazyList.isEmpty()){
                updateRows(start,end,lazyList);
            }
        }

    }

    private void updateRows(int start,int end,List<RowType> result){       
        serverDataProvider.updateRowData(start, result.subList(start, end));
        serverDataProvider.updateRowCount(result.size(), true);
    }
    
    public MServerDataSourceProvider() {
        serverDataProvider = new MAsyncDataProvider();
        lazyList = new ArrayList<RowType>();
    }


    @Override
    public void addData(RowType data) {
        // TODO Auto-generated method stub
    }


    @Override
    public void removeData(RowType data) {
        // TODO Auto-generated method stub
    }

    @Override
    public Integer getTotalRowsCount() {
        return lazyList.size();
    }


    @Override
    public void setData(Collection<RowType> data) {
        //Ajoute les données à la lazyList.
        lazyList.clear();
        lazyList.addAll(data);
        
        
        int size = lazyList.size();
        
        int end = (size <= MDataGrid.DEFAULT_PAGESIZE)? size : MDataGrid.DEFAULT_PAGESIZE;
        updateRows(0,end,lazyList);
    }


    @Override
    public void connectToDataGrid(MDataGrid<RowType> dataGrid) {
        serverDataProvider.addDataDisplay(dataGrid);
    }

}
