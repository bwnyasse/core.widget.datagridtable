/**
 * 
 */
package fr.bwn.win.client.datagrid.column;

import java.io.Serializable;

import com.google.gwt.cell.client.FieldUpdater;

/**
 *
 */
public interface MFieldUpdater<RowType extends Serializable, ColType extends Serializable> extends FieldUpdater<RowType, ColType>{

}
