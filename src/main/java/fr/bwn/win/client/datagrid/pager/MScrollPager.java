/**
 * 
 */
package fr.bwn.win.client.datagrid.pager;

import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.user.cellview.client.AbstractPager;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.view.client.HasRows;

import fr.bwn.win.client.datagrid.impl.MDataGrid;


/**
 * Classe d'implémentation du widget responsable de la pagination avec la scrollbar du navigateur. <p>
 * 
 *  La {@link MScrollPager} met à jour la taille des données contenues dans {@link} lorsque la scrollbar atteint le fond.
 *
 * @author bwnyasse
 */
public class MScrollPager extends AbstractPager {

    private static final int DEFAULT_INCREMENT = MDataGrid.DEFAULT_PAGESIZE;

    private int incrementSize = DEFAULT_INCREMENT;

    private int lastPosition = 0;

    /**
     * @param scrollPanel
     */
    public MScrollPager(final ScrollPanel scrollPanel) {
        initWidget(scrollPanel);

        // Handler des "scroll events".
        scrollPanel.addScrollHandler(new ScrollHandler() {
            @Override
            public void onScroll(ScrollEvent event) {
                
                //On ignore l'évènement si , la scrollbar est en haut.
                int oldPosition = lastPosition;
                lastPosition = scrollPanel.getVerticalScrollPosition();
                if (oldPosition >= lastPosition) {
                    return;
                }

                HasRows display = getDisplay();
                if (display == null) {
                    return;
                }
                
                int maxScrollTop = scrollPanel.getWidget().getOffsetHeight()
                        - scrollPanel.getOffsetHeight();
                if (lastPosition >= maxScrollTop) {
                    // On incrémente la page.
                    int newPageSize = Math.min(
                            display.getVisibleRange().getLength() + incrementSize,
                            display.getRowCount());
                    display.setVisibleRange(0, newPageSize);
                }
            }
        });
    }

    public int getIncrementSize() {
        return incrementSize;
     }
    
    public void setIncrementSize(int incrementSize) {
        this.incrementSize = incrementSize;
      }
    
    @Override
    protected void onRangeOrRowCountChanged() {
        //Rien à faire.
    }



}
