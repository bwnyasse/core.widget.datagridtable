/**
 * 
 */
package fr.bwn.win.client.datagrid.gin;

import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;

/**
 * @author bwnyasse
 *
 */
@GinModules({MTableGinModule.class})
public interface MTableGinInjector extends Ginjector {  
}
