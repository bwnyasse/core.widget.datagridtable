/**
 * 
 */
package fr.bwn.win.client.datagrid.datasource;

import java.io.Serializable;
import java.util.Collection;

import fr.bwn.win.client.datagrid.impl.MDataGrid;

/**
 * Interface définissant le comportement d'une provider de données pour le tableau. 
 * 
 * @author bwnyasse
 */
public interface IDataSourceProvider<RowType extends Serializable> {

    /**
     * Ajoute une donnée au provider de données. 
     * 
     * @param data
     *      la donnée à ajouter.
     */
    public void addData(RowType data);

    /**
     * Supprimer une donnée du provider de données.
     * 
     * @param data
     *  la donnée à supprimer.
     */
    public void removeData(RowType data);

    /**
     * Méthode utilisée pour retourner le nombre total de lignes sans filtrage (visibles et non
     * visibles).
     * 
     * @return Le nombre total de lignes.
     */
    public Integer getTotalRowsCount();

    /** Ajoute une liste de données au provider de données.
     * 
     * @param datas.
     *  La liste de données.
     */
    public void setData(Collection<RowType> data);

    /**
     * Méthode utilisée pour connecter le fournisseur de données à une grille de cellules.
     */
    public void connectToDataGrid(MDataGrid<RowType> dataGrid);
    
}
