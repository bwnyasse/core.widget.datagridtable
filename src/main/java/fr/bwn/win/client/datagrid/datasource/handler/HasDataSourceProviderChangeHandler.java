package fr.bwn.win.client.datagrid.datasource.handler;

import java.io.Serializable;

import fr.bwn.win.client.datagrid.datasource.model.MDataSourceModel;

/**
 * Handler de notification d'un changement dans la datasource. <p>
 * 
 * @author bwnyasse
 *
 * @param <T>
 */
public interface HasDataSourceProviderChangeHandler<RowType extends Serializable>
{

    /**
	 * Méthode utilisée pour ajouter ce handler.
	 * 
	 * @param handler
	 *        le handler exécutée quand les données d'un {@link MDataSourceModel} changent.
	 */
	public void addDataSourceProviderChangeHandler(IDataSourceProviderChangeHandler<RowType> handler);
}
