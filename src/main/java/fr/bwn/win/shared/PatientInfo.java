package fr.bwn.win.shared;

import java.io.Serializable;
import java.util.Date;


public class PatientInfo implements Comparable<PatientInfo> , Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static int nextId = 0;

    private String address;
    private Date birthday;
    private String firstName;
    private final int id;
    private String lastName;

    public PatientInfo() {
        this.id = nextId;
        nextId++;
    }

    public int compareTo(PatientInfo o) {
        return (o == null || o.firstName == null) ? -1
                : -o.firstName.compareTo(firstName);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof PatientInfo) {
            return id == ((PatientInfo) o).id;
        }
        return false;
    }

    /**
     * @return the patient's address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @return the patient's age
     */
    @SuppressWarnings("deprecation")
    public int getAge() {
        Date today = new Date();
        int age = today.getYear() - birthday.getYear();
        if (today.getMonth() > birthday.getMonth()
                || (today.getMonth() == birthday.getMonth() && today.getDate() > birthday.getDate())) {
            age--;
        }
        return age;
    }

    /**
     * @return the patient's birthday
     */
    public Date getBirthday() {
        return birthday;
    }


    /**
     * @return the patient's firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return the patient's full name
     */
    public final String getFullName() {
        return firstName + " " + lastName;
    }

    /**
     * @return the unique ID of the patient
     */
    public int getId() {
        return this.id;
    }

    /**
     * @return the patient's lastName
     */
    public String getLastName() {
        return lastName;
    }

    @Override
    public int hashCode() {
        return id;
    }

    /**
     * Set the patient's address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Set the patient's birthday.
     *
     * @param birthday the birthday
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }



    /**
     * Set the patient's first name.
     *
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Set the patient's last name.
     *
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
